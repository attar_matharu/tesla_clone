import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {Colors} from '../../constants';
import styles from './styles';

const Button = ({buttonTitle, type}) => {
  const bgColor = type === 'primary' ? Colors.black : Colors.white;
  const textColor = type === 'primary' ? Colors.white : Colors.black;
  return (
    <TouchableOpacity style={{...styles.buttonWrap, backgroundColor: bgColor}}>
      <Text style={{...styles.buttonText, color: textColor}}>
        {buttonTitle}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;
