import {StyleSheet} from 'react-native';
import fonts from '../../constants/fonts';

export default StyleSheet.create({
  buttonWrap: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'pink',
    borderRadius: 20,
    paddingVertical: 12,
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 14,
    fontFamily: fonts.BOLD,
    textTransform: 'uppercase',
  },
});
