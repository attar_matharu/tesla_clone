import React from 'react';
import {FlatList,  View} from 'react-native';
import {CarsData, DeviceDimension} from '../../constants';
import CarItem from '../carItem';
const CarsList = ({params}) => (
  <View>
    <FlatList
      data={CarsData}
      renderItem={({item, index}) => {
        return <CarItem items={item} />;
      }}
      snapToAlignment={'end'}
      decelerationRate={'fast'}
      snapToInterval={DeviceDimension.DEVICE_HEIGHT}
    />
  </View>
);

export default CarsList;
