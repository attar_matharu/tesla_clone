import React from 'react';
import { Image,  View } from 'react-native';
import { Images } from '../../constants';
import styles from './styles';

const Header = () => (
    <View style={styles.headerWrap}>
        <Image source={Images.LOGO} style={styles.logo}/>
        <Image source={Images.MENU} style={styles.menu}/>
    </View>
);

export default Header;
