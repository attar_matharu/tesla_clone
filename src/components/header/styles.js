import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  headerWrap: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 10,
    position: 'absolute',
    zIndex: 1,
    elevation: 3,
  },
  logo: {
    height: 20,
    width: 100,
    resizeMode: 'contain',
  },
  menu: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});
