import React from 'react';
import {Text, View, ImageBackground} from 'react-native';
import Button from '../button';

import styles from './styles';
import {stringObj} from '../../constants';
const CarItem = ({items}) => (
  <View style={styles.mainContainer}>
    <ImageBackground source={items.image} style={styles.container}>
      <View style={styles.subContainer}>
        <Text style={styles.name}>{items.name}</Text>
        <Text style={styles.tagline}>
          {items.tagline}{' '}
          <Text style={styles.taglineCTA}>{items.taglineCTA}</Text>
        </Text>
      </View>
      <View style={styles.endContainer}>
        <Button buttonTitle={stringObj.orderNow} type="primary" />
        <Button buttonTitle={stringObj.existingInventory} type="secondary" />
      </View>
    </ImageBackground>
  </View>
);

export default CarItem;
