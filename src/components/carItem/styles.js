import {StyleSheet} from 'react-native';
import {Colors, DeviceDimension, Fonts} from '../../constants';
import fonts from '../../constants/fonts';
export default StyleSheet.create({
  mainContainer: {
    height: DeviceDimension.DEVICE_HEIGHT,
    width: DeviceDimension.DEVICE_WIDTH,
  },

  container: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
  },
  subContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20%',
  },
  name: {
    fontSize: 35,
    fontFamily: Fonts.BOLD,
  },
  tagline: {
    fontSize: 16,
    color: Colors.grey,
    fontFamily: fonts.REGULAR,
  },
  taglineCTA: {
    textDecorationLine: 'underline',
  },
  endContainer: {
    width: DeviceDimension.DEVICE_WIDTH,
    position: 'absolute',
    bottom: 60,
    padding: 10,
  },
});
