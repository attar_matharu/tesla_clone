const Strings = {
  modelS: 'Model S',
  model3: 'Model 3',
  modelX: 'Model X',
  modelY: 'Model Y',
  startingat$69420: 'Starting at $69,420',
  orderOnlinefor: 'Order Online for',
  touchlessDelivery: 'Touchless Delivery',
  orderNow:"order now",
  existingInventory:"existing inventory"
};
export default Strings;
