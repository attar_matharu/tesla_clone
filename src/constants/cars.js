import Strings from "./strings";
import Images from "./images";
export default [{
  name: Strings.modelS,
  tagline: Strings.startingat$69420,
  image:Images.MODELS,
}, {
  name: Strings.model3,
  tagline:Strings.orderOnlinefor,
  taglineCTA: Strings.touchlessDelivery,
  image: Images.MODEL3,
}, {
  name: Strings.modelX,
  tagline:Strings.orderOnlinefor,
  taglineCTA: Strings.touchlessDelivery,
  image: Images.MODELX,
}, {
  name:Strings.modelY,
  tagline:Strings.orderOnlinefor,
  taglineCTA: Strings.touchlessDelivery,
  image:Images.MODELY,
}];
