import DeviceDimension from './dimensions';
import Fonts from './fonts';
import Colors from './colors';
import Images from './images';
import stringObj from './strings';
import CarsData from './cars';
export {Fonts, DeviceDimension, Colors, Images, stringObj, CarsData};
