const Images = {
  LOGO: require('../assets/images/logo.png'),
  MENU: require('../assets/images/menu.png'),
  MODEL3: require('../assets/images/Model3.jpeg'),
  MODELS: require('../assets/images/ModelS.jpeg'),
  MODELX: require('../assets/images/ModelX.jpeg'),
  MODELY: require('../assets/images/ModelY.jpeg'),
  SOLARPANELS: require('../assets/images/SolarPanels.jpeg'),
  SOLARROOF: require('../assets/images/SolarRoof.jpeg'),
};
export default Images;
