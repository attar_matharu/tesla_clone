import React from 'react';
import {View} from 'react-native';
import CarsList from './components/carsList';
import Header from './components/header';
const App = () => (
  <View>
    <Header />
    <CarsList />
  </View>
);

export default App;
